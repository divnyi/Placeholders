//
//  Dependency.swift
//  Placeholders
//
//  Created by Oleksii Horishnii on 7/19/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol Dependency {
    func useCase() -> UseCase
    func itemRepository() -> ItemRepository
    func presenter() -> Presenter
}
