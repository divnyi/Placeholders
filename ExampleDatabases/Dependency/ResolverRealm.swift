//
//  DependencyRealm.swift
//  ExampleRealm
//
//  Created by Oleksii Horishnii on 7/19/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import UIKit

class Resolver: ResolverCommon {
    override func itemRepository() -> ItemRepository {
        return ItemProvider.shared
    }
}
