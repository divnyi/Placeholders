//
//  Dependency.swift
//  Placeholders
//
//  Created by Oleksii Horishnii on 7/19/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import UIKit

class ResolverCommon: NSObject, Dependency {
    static let shared = Resolver()

    func useCase() -> UseCase {
        return Interactor(dependency: self)
    }

    // needs to be overriden
    func itemRepository() -> ItemRepository {
        fatalError()
    }
    
    func presenter() -> Presenter {
        return PresenterImplementation(dependency: self)
    }
}
