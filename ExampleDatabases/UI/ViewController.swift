//
//  ViewController.swift
//  ExampleCoreData
//
//  Created by Oleksii Horishnii on 5/8/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Placeholders

protocol Presenter {
    var rows: RowsProvider<RowModel> { get }
}

class ViewController: UIViewController {
    @IBOutlet weak var placeholder: PlaceholderView!
    var tableVC: PlaceholderTableVC!
    var presenter: Presenter = Resolver.shared.presenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableVC = PlaceholderTableVC.create()
        
        // 6. Transform View Model into CellGenerator, that explains how to
        // create, update the cell and how to react on cell actions
        let cellGenerators = self.presenter.rows.transform { (rowModel) -> CellGenerator in
            CellGenerator(create: .generator({ () -> UILabel in
                return UILabel(frame: CGRect.zero)
            }), update: { (label: UILabel, _) in
                label.text = rowModel.text
                label.backgroundColor = rowModel.bgColor
                label.textAlignment = rowModel.alignment == .left ? .left : .center
            }, clicked: rowModel.clicked,
               deleted: rowModel.deleted)
        }
        
        self.tableVC.rowsProvider = cellGenerators
        
        self.placeholder.insertedView = self.tableVC
    }


}

