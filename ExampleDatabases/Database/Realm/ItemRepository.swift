//
//  ItemRepository.swift
//  ExampleRealm
//
//  Created by Oleksii Horishnii on 7/19/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import UIKit
import RealmSwift
import Placeholders
import PlaceholdersRealm

class RealmItem: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var date: Date = Date()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func fromPONSO(_ ponso: Item) {
        self.date = ponso.date
    }
    func toPONSO() -> Item {
        return Item(id: Int(self.id), date: self.date)
    }
}

class ItemProvider: ItemRepository {
    static let shared = ItemProvider()
    let realm = try! Realm()

    private func new(_ item: Item) -> RealmItem {
        let dbobj = RealmItem()
        dbobj.id = item.id
        return dbobj
    }
    
    private func find(_ item: Item) -> RealmItem? {
        return self.realm.object(ofType: RealmItem.self, forPrimaryKey: item.id)
    }
    
    func save(_ item: Item) {
        let dbitem = self.find(item) ?? self.new(item)
        try! self.realm.write {
            dbitem.fromPONSO(item)
            self.realm.add(dbitem, update: true)
        }
    }
    
    func delete(_ item: Item) {
        if let dbobj = self.find(item) {
            try! self.realm.write {
                self.realm.delete(dbobj)
            }
        }
    }
    
    func allItems() -> RowsProvider<Item> {
        // 1. Get RowsProvider from Results<RealmItem>
        let dbRows = self.realm.objects(RealmItem.self)
            .sorted(by: [SortDescriptor(keyPath: "date", ascending: false)])
            .rowsProvider()
        // 2. Transform it to plain NSObject model item RowsProvider
        let itemRows = dbRows.transform { $0.toPONSO() }
        return itemRows
    }
}
