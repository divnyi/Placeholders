//
//  Presenter.swift
//  ExampleCoreData
//
//  Created by Oleksii Horishnii on 5/11/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Placeholders

protocol UseCase {
    var allItems: RowsProvider<Item> { get }
    func addNewItem()
    func deleteItem(item: Item)
}

class PresenterImplementation: NSObject, Presenter {
    var useCase: UseCase
    
    convenience init(dependency: Dependency = Resolver.shared) {
        self.init(useCase: dependency.useCase())
    }
    
    init(useCase: UseCase) {
        self.useCase = useCase
        // 3. Transform Model item into View Model item, with data relevant to UI
        let itemModels = useCase.allItems.transform({ (item) -> RowModel in
            let model = RowModel.from(item: item)
            model.deleted = {
                useCase.deleteItem(item: item)
            }
            return model
        })
        // 4. Also create View Model for static item on top
        let newItem = RowModel.addItemCell()
        newItem.clicked = {
            useCase.addNewItem()
        }
        // 5. Add static item to dynamic items by adding RowsProvicers of same type
        let rows = RowsProvider.from(singleItem: newItem) + itemModels
        self.rows = rows
    }
    
    //MARK:- API
    var rows: RowsProvider<RowModel>
}
