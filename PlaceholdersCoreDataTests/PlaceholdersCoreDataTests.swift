//
//  PlaceholderCoreDataTests.swift
//  PlaceholderCoreDataTests
//
//  Created by Oleksii Horishnii on 9/14/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import XCTest
import Placeholders
import PlaceholdersCoreData
import CoreData

class PlaceholdersCoreDataTests: XCTestCase {
    let manager = DatabaseManager.shared
    var provider: RowsProvider<Entity>!
    
    override func setUp() {
        super.setUp()
        
        self.provider?.observers.removeAll()
        self.provider = nil
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Entity")
        let items: [NSManagedObject] = try! DatabaseManager.shared.managedObjectContext.fetch(deleteFetch) as! [NSManagedObject]
        
        for item in items {
            DatabaseManager.shared.managedObjectContext.delete(item);
        }
        try! DatabaseManager.shared.managedObjectContext.save();

    }
    
    func testCreateSingleItem() {
        let params = FetchRequestParameters(entityName: "Entity", managedObjectContext: DatabaseManager.shared.managedObjectContext, sortDescriptors: [NSSortDescriptor(key: "id", ascending: true)])
        let provider = RowsProvider<Entity>.from(coreDataParams: params)

        let insertExpectation = expectation(description: "Inserted")
        let listener = RowsObserverSimple { updateDelegate in
            updateDelegate.insertFn = { paths in
                insertExpectation.fulfill()
            }
        }
        let _ = provider.observers.add(listener)

        let _ = Entity.init(entity: Entity.entity(), insertInto: DatabaseManager.shared.managedObjectContext)
        try! DatabaseManager.shared.managedObjectContext.save();

        waitForExpectations(timeout: 0.3, handler: nil)
    }
    
    func testCreate10Items() {
        let params = FetchRequestParameters(entityName: "Entity", managedObjectContext: DatabaseManager.shared.managedObjectContext, sortDescriptors: [NSSortDescriptor(key: "id", ascending: true)])
        let provider = RowsProvider<Entity>.from(coreDataParams: params)

        var expectations: [XCTestExpectation] = []
        for idx in 0..<10 {
            expectations.append(expectation(description: "Inserted at idx = \(idx)"))
        }

        let listener = RowsObserverSimple { updateDelegate in
            updateDelegate.insertFn = { paths in
                for path in paths {
                    let expectation = expectations[path.row];
                    expectation.fulfill()
                }
            }
        }
        let _ = provider.observers.add(listener)
        
        for _ in 0..<10 {
            Entity.init(entity: Entity.entity(), insertInto: DatabaseManager.shared.managedObjectContext)
        }
        try! DatabaseManager.shared.managedObjectContext.save();
        
        waitForExpectations(timeout: 0.3, handler: nil)
    }
    
    func testDeleteItems() {
        let params = FetchRequestParameters(entityName: "Entity", managedObjectContext: DatabaseManager.shared.managedObjectContext, sortDescriptors: [NSSortDescriptor(key: "id", ascending: true)])
        let provider = RowsProvider<Entity>.from(coreDataParams: params)

        let expectationDelete3 = expectation(description: "item with id = 3 deleted")
        let expectationDelete7 = expectation(description: "item with id = 7 deleted")
        let listener = RowsObserverSimple { updateDelegate in
            updateDelegate.deleteFn = { paths in
                for path in paths {
                    if (path.row == 3) {
                        expectationDelete3.fulfill()
                    }
                    if (path.row == 7) {
                        expectationDelete7.fulfill()
                    }
                }
            }
        }
        let _ = provider.observers.add(listener)
        
        var items: [Entity] = []
        for idx in 0..<10 {
            let newItem = Entity.init(entity: Entity.entity(), insertInto: DatabaseManager.shared.managedObjectContext)
            newItem.id = Int32(idx)
            items.append(newItem)
        }
        try! DatabaseManager.shared.managedObjectContext.save();

        DatabaseManager.shared.managedObjectContext.delete(items[3])
        DatabaseManager.shared.managedObjectContext.delete(items[7])
        try! DatabaseManager.shared.managedObjectContext.save();

        waitForExpectations(timeout: 0.3, handler: nil)
    }
}
