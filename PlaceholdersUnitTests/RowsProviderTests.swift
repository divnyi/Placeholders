//
//  RowsProviderTests.swift
//  RowsProviderTests
//
//  Created by Oleksii Horishnii on 7/20/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import XCTest
import Placeholders

class RowsProviderTests: XCTestCase {
    var arr: [[Int]]!
    var rowsProvider: RowsProvider<Int>!
    
    override func setUp() {
        super.setUp()
        
        self.arr = [[1, 2, 3, 4, 5, 6, 7], [10, 20, 30], [100, 200, 300, 400]]
        self.rowsProvider = RowsProvider.from(rows: self.arr)
    }
    
    override func tearDown() {
        self.rowsProvider.observers.removeAll()
    }
    
    func testGetter() {
        for (section, items) in self.arr.enumerated() {
            for (row, item) in items.enumerated() {
                XCTAssertEqual(item, self.rowsProvider[IndexPath(row: row, section: section)])
            }
        }
    }

    func testToArray() {
        XCTAssertEqual(self.rowsProvider.toArray(), self.arr)
    }
    
    func testSafeGetter() {
        XCTAssertThrowsError(try self.rowsProvider.item(path: IndexPath(row: 999, section: 999)))
    }
    
    func testMap() {
        let stringRows = self.rowsProvider.transform { (val) -> String in
            return "\(val)"
        }
        for (section, items) in self.arr.enumerated() {
            for (row, item) in items.enumerated() {
                XCTAssertEqual("\(item)", stringRows[IndexPath(row: row, section: section)])
            }
        }
    }
    
    func testCombineAddingSections() {
        let rowsProviders = self.arr.map { (subArray) -> RowsProvider<Int> in
            return RowsProvider.from(array: subArray)
        }
        for i in 0..<3 {
            // sanity check
            XCTAssertEqual(rowsProviders[i].toArray(), [self.arr[i]])
        }
        let combined = RowsProvider.from(rowsProviders: rowsProviders)
        
        XCTAssertEqual(combined.toArray(), self.rowsProvider.toArray())
    }

}
