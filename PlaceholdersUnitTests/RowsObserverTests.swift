//
//  RowsObserverTests.swift
//  PlaceholderTests
//
//  Created by Oleksii Horishnii on 7/25/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import XCTest
import Placeholders

class RowsObserverTests: XCTestCase {
    var arr: [[Int]]!
    var rowsProvider: RowsProvider<Int>!
    
    override func setUp() {
        super.setUp()
        
        self.arr = [[1, 2, 3, 4, 5, 6, 7], [10, 20, 30], [100, 200, 300, 400]]
        self.rowsProvider = RowsProvider.from(rows: self.arr)
    }
    
    override func tearDown() {
        self.rowsProvider.observers.removeAll()
    }

    func testUpdateDelegate() {
        var willChangeContentBool = false
        var didChangeContentBool = false
        var insertBool = false
        var deleteBool = false
        var updateBool = false
        var sectionInsertBool = false
        var sectionDeleteBool = false
        var updateUIBool = false
        
        let listener = RowsObserverSimple(initFn: { (listener) in
            listener.willChangeContentFn = { willChangeContentBool = true }
            listener.didChangeContentFn = { didChangeContentBool = true }
            listener.insertFn = { (_) in insertBool = true }
            listener.deleteFn = { (_) in deleteBool = true }
            listener.updateFn = { (_) in updateBool = true }
            listener.sectionInsertFn = { (_) in sectionInsertBool = true }
            listener.sectionDeleteFn = { (_) in sectionDeleteBool = true }
            listener.updateUIFn = { updateUIBool = true }
        })
        let _ = self.rowsProvider.observers.add(listener)
        
        XCTAssertFalse(willChangeContentBool)
        self.rowsProvider.willChangeContent()
        XCTAssertTrue(willChangeContentBool)
        
        XCTAssertFalse(didChangeContentBool)
        self.rowsProvider.didChangeContent()
        XCTAssertTrue(didChangeContentBool)
        
        XCTAssertFalse(insertBool)
        self.rowsProvider.insert(paths: [])
        XCTAssertTrue(insertBool)
        
        XCTAssertFalse(deleteBool)
        self.rowsProvider.delete(paths: [])
        XCTAssertTrue(deleteBool)
        
        XCTAssertFalse(updateBool)
        self.rowsProvider.update(paths: [])
        XCTAssertTrue(updateBool)
        
        XCTAssertFalse(sectionInsertBool)
        self.rowsProvider.sectionInsert(sections: [])
        XCTAssertTrue(sectionInsertBool)
        
        XCTAssertFalse(sectionDeleteBool)
        self.rowsProvider.sectionDelete(sections: [])
        XCTAssertTrue(sectionDeleteBool)
        
        XCTAssertFalse(updateUIBool)
        self.rowsProvider.updateUI()
        XCTAssertTrue(updateUIBool)
    }
    
    func testUpdateDelegateMap() {
        var willChangeContentBool = false
        var didChangeContentBool = false
        var insertBool = false
        var deleteBool = false
        var updateBool = false
        var sectionInsertBool = false
        var sectionDeleteBool = false
        var updateUIBool = false
        
        let listener = RowsObserverSimple(initFn: { (listener) in
            listener.willChangeContentFn = { willChangeContentBool = true }
            listener.didChangeContentFn = { didChangeContentBool = true }
            listener.insertFn = { (_) in insertBool = true }
            listener.deleteFn = { (_) in deleteBool = true }
            listener.updateFn = { (_) in updateBool = true }
            listener.sectionInsertFn = { (_) in sectionInsertBool = true }
            listener.sectionDeleteFn = { (_) in sectionDeleteBool = true }
            listener.updateUIFn = { updateUIBool = true }
        })
        let _ = self.rowsProvider.observers.add(listener)
        
        XCTAssertFalse(willChangeContentBool)
        self.rowsProvider.willChangeContent()
        XCTAssertTrue(willChangeContentBool)
        
        XCTAssertFalse(didChangeContentBool)
        self.rowsProvider.didChangeContent()
        XCTAssertTrue(didChangeContentBool)
        
        XCTAssertFalse(insertBool)
        self.rowsProvider.insert(paths: [])
        XCTAssertTrue(insertBool)
        
        XCTAssertFalse(deleteBool)
        self.rowsProvider.delete(paths: [])
        XCTAssertTrue(deleteBool)
        
        XCTAssertFalse(updateBool)
        self.rowsProvider.update(paths: [])
        XCTAssertTrue(updateBool)
        
        XCTAssertFalse(sectionInsertBool)
        self.rowsProvider.sectionInsert(sections: [])
        XCTAssertTrue(sectionInsertBool)
        
        XCTAssertFalse(sectionDeleteBool)
        self.rowsProvider.sectionDelete(sections: [])
        XCTAssertTrue(sectionDeleteBool)
        
        XCTAssertFalse(updateUIBool)
        self.rowsProvider.updateUI()
        XCTAssertTrue(updateUIBool)
    }
    
    func testUpdateDelegateAddingSections() {
        var rowsProviders = self.arr.map { (subArray) -> RowsProvider<Int> in
            return RowsProvider.from(array: subArray)
        }
        rowsProviders.append(RowsProvider.from(rows: self.arr))
        for i in 0..<3 {
            // sanity check
            XCTAssertEqual(rowsProviders[i].toArray(), [self.arr[i]])
        }
        XCTAssertEqual(rowsProviders[3].toArray(), self.arr)
        
        let testPaths0 = [IndexPath(row: 1, section: 0), IndexPath(row: 2, section: 0)]
        let testPaths1 = [IndexPath(row: 1, section: 1), IndexPath(row: 2, section: 1)]
        let testPaths2 = [IndexPath(row: 1, section: 2), IndexPath(row: 2, section: 2)]
        let testPaths3 = [IndexPath(row: 1, section: 3), IndexPath(row: 2, section: 3)]
        let testPaths4 = [IndexPath(row: 1, section: 4), IndexPath(row: 2, section: 4)]
        let testPaths5 = [IndexPath(row: 1, section: 5), IndexPath(row: 2, section: 5)]
        var indexPaths: [IndexPath]?
        
        let combined = RowsProvider.from(rowsProviders: rowsProviders)
        let listener = RowsObserverSimple { updateDelegate in
            updateDelegate.updateFn = { paths in indexPaths = paths }
        }
        let _ = combined.observers.add(listener)
        
        XCTAssertNil(indexPaths)
        
        rowsProviders[0].update(paths: testPaths0)
        XCTAssertEqual(indexPaths, testPaths0)
        rowsProviders[1].update(paths: testPaths0)
        XCTAssertEqual(indexPaths, testPaths1)
        rowsProviders[2].update(paths: testPaths0)
        XCTAssertEqual(indexPaths, testPaths2)
        
        rowsProviders[3].update(paths: testPaths0)
        XCTAssertEqual(indexPaths, testPaths3)
        rowsProviders[3].update(paths: testPaths1)
        XCTAssertEqual(indexPaths, testPaths4)
        rowsProviders[3].update(paths: testPaths2)
        XCTAssertEqual(indexPaths, testPaths5)
    }
    
    func testAddingSectionsComplex() {
        var rowsProviders: [RowsProvider<Int>] = []
        let provider = RowsProvider(sections: { () -> Int in
            return self.arr.count
        }, rows: { (section) -> Int in
            if (section >= self.arr.count) {
                return 0
            }
            return self.arr[section].count
        }, item: { indexPath -> Int in
            if (indexPath.section >= self.arr.count ||
                indexPath.row >= self.arr[indexPath.section].count) {
                throw RowsProvider<Int>.Errors.outOfBounds
            }
            return self.arr[indexPath.section][indexPath.row]
        })
        let provider2 = RowsProvider(sections: { () -> Int in
            return self.arr.count
        }, rows: { (section) -> Int in
            if (section >= self.arr.count) {
                return 0
            }
            return self.arr[section].count
        }, item: { indexPath -> Int in
            if (indexPath.section >= self.arr.count ||
                indexPath.row >= self.arr[indexPath.section].count) {
                throw RowsProvider<Int>.Errors.outOfBounds
            }
            return self.arr[indexPath.section][indexPath.row]
        })
        rowsProviders.append(provider)
        rowsProviders.append(provider2)
        
        // sanity check
        XCTAssertEqual(rowsProviders[0].toArray(), self.arr)
        XCTAssertEqual(rowsProviders[1].toArray(), self.arr)
        
        let combined = RowsProvider.from(rowsProviders: rowsProviders)
        
        XCTAssertEqual(combined.sections(), 6)
        self.arr.append([9999, 101010])
        XCTAssertEqual(combined.sections(), 8)
        
        // we can it fact get new values, even before update event was triggered
        XCTAssertEqual(combined[IndexPath(row: 0, section: 3)], 9999)
        XCTAssertEqual(combined[IndexPath(row: 1, section: 7)], 101010)
        
        var indexPaths: [IndexPath]?
        var sections: [Int]?
        var updatesBegan = 0
        
        let listener = RowsObserverSimple { updateDelegate in
            updateDelegate.updateFn = { paths in indexPaths = paths }
            updateDelegate.sectionInsertFn = { sect in sections = sect }
            updateDelegate.sectionDeleteFn = { sect in sections = sect }
            updateDelegate.willChangeContentFn = { updatesBegan += 1 }
            updateDelegate.didChangeContentFn = { updatesBegan -= 1 }
        }
        let _ = combined.observers.add(listener)
        
        XCTAssertEqual(updatesBegan, 0)
        rowsProviders[1].willChangeContent()
        XCTAssertEqual(updatesBegan, 1)
        // at this point we should get old adresses when updating
        
        rowsProviders[1].update(paths: [IndexPath(row: 0, section: 3)])
        XCTAssertEqual(indexPaths, [IndexPath(row: 0, section: 6)])
        
        rowsProviders[0].willChangeContent() // should do nothing
        XCTAssertEqual(updatesBegan, 1)
        
        rowsProviders[0].sectionInsert(sections: [3])
        XCTAssertEqual(sections, [3])
        
        rowsProviders[0].didChangeContent() // should do nothing
        XCTAssertEqual(updatesBegan, 1)
        
        // still getting old adresses, we didn't end updating rowsProviders[1]
        rowsProviders[1].sectionInsert(sections: [3])
        XCTAssertEqual(sections, [6])
        
        rowsProviders[1].update(paths: [IndexPath(row: 0, section: 3)])
        XCTAssertEqual(indexPaths, [IndexPath(row: 0, section: 6)])
        
        rowsProviders[0].didChangeContent()
        XCTAssertEqual(updatesBegan, 0)
        // at this point we should get new adresses
        
        rowsProviders[1].sectionInsert(sections: [3])
        XCTAssertEqual(sections, [7])
        
        rowsProviders[1].update(paths: [IndexPath(row: 0, section: 3)])
        XCTAssertEqual(indexPaths, [IndexPath(row: 0, section: 7)])
        
        // we need actual physical changes
        //    - sectionShifts calculation is
        //      based on RowsProvider.section() call
        self.arr.removeLast()
        
        XCTAssertEqual(updatesBegan, 0)
        rowsProviders[1].willChangeContent()
        XCTAssertEqual(updatesBegan, 1)
        // at this point we should get old adresses when updating
        rowsProviders[1].update(paths: [IndexPath(row: 0, section: 3)])
        XCTAssertEqual(indexPaths, [IndexPath(row: 0, section: 7)])
        
        rowsProviders[1].update(paths: [IndexPath(row: 0, section: 3)])
        XCTAssertEqual(indexPaths, [IndexPath(row: 0, section: 7)])
        
        rowsProviders[0].didChangeContent()
        XCTAssertEqual(updatesBegan, 0)
        // at this point we should get new adresses
        
        rowsProviders[1].update(paths: [IndexPath(row: 0, section: 3)])
        XCTAssertEqual(indexPaths, [IndexPath(row: 0, section: 6)])
    }
}
