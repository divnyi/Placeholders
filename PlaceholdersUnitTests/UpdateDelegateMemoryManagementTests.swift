//
//  UpdateDelegateMemoryManagement.swift
//  PlaceholderTests
//
//  Created by Oleksii Horishnii on 7/25/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import XCTest
import Placeholders

class UpdateDelegateMemoryManagementTests: XCTestCase {
    var arr: [[Int]]!
    var rowsProvider: RowsProvider<Int>!
    
    override func setUp() {
        super.setUp()
        
        self.arr = [[1, 2, 3, 4, 5, 6, 7], [10, 20, 30], [100, 200, 300, 400]]
        self.rowsProvider = RowsProvider.from(rows: self.arr)
    }
    
    override func tearDown() {
        self.rowsProvider.observers.removeAll()
    }

    func testSimple() {
        func scope() {
            let temporary = self.rowsProvider.transform { (arg) -> String in return "\(arg)" }
            let _ = temporary // remove warning
            XCTAssertEqual(self.rowsProvider.observers.items().count, 1)
        }
        scope()
        XCTAssertEqual(self.rowsProvider.observers.items().count, 0)
    }
    
    func testTranslationMatrix() {
        func scope () {
            let newRows = self.rowsProvider.transform { (arg) -> String in return "\(arg)" }
            self.rowsProvider.observers.removeAll()
            let translationMatrix = RowsObserverTranslationMatrix(target: newRows, sectionShift: { (arg) -> Int in
                return arg
            }, rowShift: { (arg) -> IndexPath in
                return arg
            }, synchronizer: nil)
            let _ = self.rowsProvider.observers.add(translationMatrix)
            XCTAssertEqual(self.rowsProvider.observers.items().count, 1)
        }
        scope()
        XCTAssertEqual(self.rowsProvider.observers.items().count, 0)
    }
    
    func testRowsUpdateListener() {
        func scope () {
            let a = RowsProvider.from(singleItem: 1)
            let listener = RowsObserverSimple(initFn: { (listener) in
                listener.willChangeContentFn = {
                    let _ = a.sections()
                }
                listener.didChangeContentFn = {
                    let _ = a.sections()
                }
            })
            let _ = self.rowsProvider.observers.add(listener)
            XCTAssertEqual(self.rowsProvider.observers.items().count, 1)
        }
        scope()
        XCTAssertEqual(self.rowsProvider.observers.items().count, 0)
    }
}
