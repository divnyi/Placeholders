Pod::Spec.new do |s|
  s.name              = 'PlaceholdersCoreData'
  s.version           = '0.1.3'
  s.summary           = 'CoreData bindings for https://gitlab.com/divnyi/Placeholders library.'
  s.description       = 'CoreData bindings for https://gitlab.com/divnyi/Placeholders library. Allows to obtain RowsProvider<Type> directly from CoreData, reacting on updates for observed entities.'

  s.author            = { 'Oleksii Horishnii' => 'oleksii.horishnii@gmail.com' }
  s.license           = { :type => 'WTFPL', :file => 'LICENSE' }
  s.homepage          = "https://gitlab.com/divnyi/Placeholders"

  s.platform          = :ios


  s.source = {
    :git => "https://gitlab.com/divnyi/Placeholders.git",
    :tag => "#{s.version}"
  }
  s.source_files = 'PlaceholdersCoreData/**/*.swift'
  s.ios.deployment_target = '8.0'

  s.dependency 'Placeholders'

end
