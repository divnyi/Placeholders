//
//  WeakArray.swift
//  Placeholders
//
//  Created by Oleksii Horishnii on 3/22/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import Foundation

public protocol WeakItem {
    func remove()
    func testIfAlive() -> Bool
    func obtain<Type>() -> Type?
}

public class SimpleWeakItem: WeakItem {
    public weak var item: AnyObject?
    public func remove() {
        // WeakArray will filter it out on next .items() iteration
        self.item = nil
    }
    public func testIfAlive() -> Bool {
        return self.item != nil
    }
    public func obtain<Type>() -> Type? {
        return item as? Type
    }
}

public class WeakArray<Type> {
    private var weakItems: [WeakItem] = []
    public func items() -> [Type] {
        let actualItems = self.weakItems.filter { (weakItem) -> Bool in
            return weakItem.testIfAlive()
        }
        let unwrappedItems: [Type] = actualItems.compactMap { return $0.obtain() }
        return unwrappedItems
    }
    public func add(_ item: Type) -> WeakItem {
        if let weakItem = item as? WeakItem {
            return self.add(weakItem: weakItem)
        }
        let weakItem = SimpleWeakItem()
        weakItem.item = item as AnyObject
        self.weakItems.append(weakItem)
        return weakItem
    }
    private func add(weakItem: WeakItem) -> WeakItem {
        self.weakItems.append(weakItem)
        return weakItem
    }
    public func removeAll() {
        self.weakItems.removeAll()
    }
}
