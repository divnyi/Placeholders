//
//  TableContents+ToArray.swift
//  Placeholders
//
//  Created by Oleksii Horishnii on 1/31/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import Foundation

extension RowsProvider {
    /**
     This function is not lazy and generally
     should not be used except for debug purposes
     
     - Returns: 2d array with all the items
     */
    public func toArray() -> [[Type]] {
        var sectionArray: [[Type]] = []
        for section in 0..<self.sections() {
            var rowArray: [Type] = []
            for row in 0..<self.rows(section: section) {
                let element = self[IndexPath(row: row, section: section)]
                rowArray.append(element)
            }
            sectionArray.append(rowArray)
        }
        return sectionArray
    }
    
    /**
     Transforms existing RowsProvider\<OldType\> into RowsProvider\<NewType\>.
     
     - Parameter transform: function that will be applied to RowsProvider\<OldType\>.item each time RowsProvider\<NewType\>.item is called
     
     - Returns: RowsProvider\<NewType\>, where each element is transform(oldElement)
    */
    public func transform<NewType>(_ transform: @escaping (Type) -> NewType) -> RowsProvider<NewType> {
        let retval = RowsProvider<NewType>(sections: self.sectionsFn,
                                           rows: self.rowsFn)
        { (path) -> NewType in
            return transform(self[path])
        }
        let _ = self.observers.add(retval)
        return retval
    }
}
