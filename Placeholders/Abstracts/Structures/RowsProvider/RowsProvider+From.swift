//
//  TableContents+FromArray.swift
//  Placeholders
//
//  Created by Oleksii Horishnii on 1/31/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import Foundation

extension RowsProvider {
    public static func from(singleItem: Type) -> RowsProvider {
        return RowsProvider(sections: { () -> Int in
            return 1
        }, rows: { (_) -> Int in
            return 1
        }, item: { _ -> Type in
            return singleItem
        })
    }
    
    public static func from(array: [Type]) -> RowsProvider {
        return RowsProvider(sections: { () -> Int in
            return 1
        }, rows: { (_) -> Int in
            return array.count
        }, item: { indexPath -> Type in
            if (indexPath.row >= array.count) {
                throw RowsProvider.Errors.outOfBounds
            }
            return array[indexPath.row]
        })
    }
    
    public static func from(rows: [[Type]]) -> RowsProvider {
        return RowsProvider(sections: { () -> Int in
            return rows.count
        }, rows: { (section) -> Int in
            if (section >= rows.count) {
                return 0
            }
            return rows[section].count
        }, item: { indexPath -> Type in
            if (indexPath.section >= rows.count ||
                indexPath.row >= rows[indexPath.section].count) {
                throw RowsProvider.Errors.outOfBounds
            }
            return rows[indexPath.section][indexPath.row]
        })
    }
    
    public static func from(rowsProviders: [RowsProvider<Type>]) -> RowsProvider<Type> {
        var sectionShifts: [Int]!
        func reCalculateSectionShift() {
            var rowsTotal = 0
            var retval: [Int] = []
            for rows in rowsProviders {
                retval.append(rowsTotal)
                rowsTotal += rows.sections()
            }
            sectionShifts = retval
        }
        
        let retval = RowsProvider<Type>(sections: { () -> Int in
            return rowsProviders.reduce(0, { (sum, rows) -> Int in
                return sum + rows.sections()
            })
        }, rows: { (section) -> Int in
            var sectionLeft = section
            for rows in rowsProviders {
                if (sectionLeft < rows.sections()) {
                    return rows.rows(section: sectionLeft)
                }
                sectionLeft -= rows.sections()
            }
            return 0
        }, item: { (indexPath: IndexPath) -> Type in
            var sectionLeft = indexPath.section
            for rows in rowsProviders {
                if (sectionLeft < rows.sections()) {
                    return try! rows.item(path: IndexPath(row: indexPath.row, section: sectionLeft))
                }
                sectionLeft -= rows.sections()
            }
            throw Errors.outOfBounds
        })
        
        reCalculateSectionShift()
        let synchronizer = RowsUpdateSynchronizer()
        synchronizer.willChangeContentFn = { retval.willChangeContent() }
        synchronizer.didChangeContentFn = {
            retval.didChangeContent()
            reCalculateSectionShift()
        }
        for (rowIndex, rows) in rowsProviders.enumerated() {
            let transitionMatrix = RowsObserverTranslationMatrix(target: retval, sectionShift: { section in
                return sectionShifts[rowIndex]+section
            }, rowShift: nil, synchronizer: synchronizer)
            let _ = rows.observers.add(transitionMatrix)
        }
        return retval
    }
}
