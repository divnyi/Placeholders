//
//  TableContentUpdater.swift
//  Placeholders
//
//  Created by Oleksii Horishnii on 1/29/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import Foundation
import UIKit

/*
 Some data grouped into sections and rows;
 Supports lazy initialization;
 */
public class RowsProvider<Type>: RowsObserverProxy {
    //MARK:- internals
    private(set) var sectionsFn: (() -> Int)
    private(set) var rowsFn: ((_ section: Int) -> Int)
    private(set) var itemFn: ((_ path: IndexPath) throws -> Type)
    
    //MARK:- errors
    public enum Errors: Error {
        case outOfBounds
    }

    //MARK:- constructor
    public init(sections: @escaping (() -> Int),
         rows: @escaping ((_ section: Int) -> Int),
         item: @escaping ((_ path: IndexPath) throws -> Type)) {
        self.sectionsFn = sections
        self.rowsFn = rows
        self.itemFn = item
        self.observers = WeakArray<RowsObserver>()
    }
    
    //MARK:- update system
    // use .observers.add(item: ...) to subscribe for updates
    public var observers: WeakArray<RowsObserver>

    //MARK:- getters
    public func sections() -> Int {
        return self.sectionsFn()
    }
    public func rows(section: Int) -> Int {
        return self.rowsFn(section)
    }
    public func item(path: IndexPath) throws -> Type {
        return try self.itemFn(path)
    }
    public func safeItem(path: IndexPath) -> Type? {
        do {
            return try self.itemFn(path)
        } catch {
            return nil
        }
    }
    public subscript(path: IndexPath) -> Type {
        return try! self.item(path: path)
    }
}

public func +<SameType>(left: RowsProvider<SameType>, right: RowsProvider<SameType>) -> RowsProvider<SameType> {
    return RowsProvider<SameType>.from(rowsProviders: [left, right]) as RowsProvider<SameType>
}

