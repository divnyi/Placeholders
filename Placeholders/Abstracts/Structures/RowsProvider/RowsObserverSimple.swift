//
//  RowsUpdateListener.swift
//  Placeholders
//
//  Created by Oleksii Horishnii on 7/26/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import UIKit

/**
 Simple class that implements RowsObserver and contains a number of blocks
 Useful if you don't want to implement RowsObserver yourself, and want to just pass some blocks
 You need to retain this object yourself, as WeakArray won't do it for you
 */
public class RowsObserverSimple: RowsObserver {
    public func willChangeContent() { self.willChangeContentFn?() }
    public var willChangeContentFn: (() -> Void)?
    public func didChangeContent() { self.didChangeContentFn?() }
    public var didChangeContentFn: (() -> Void)?
    
    public func insert(paths: [IndexPath]) { self.insertFn?(paths) }
    public var insertFn: ((_ paths: [IndexPath]) -> Void)?
    public func delete(paths: [IndexPath]) { self.deleteFn?(paths) }
    public var deleteFn: ((_ paths: [IndexPath]) -> Void)?
    public func update(paths: [IndexPath]) { self.updateFn?(paths) }
    public var updateFn: ((_ paths: [IndexPath]) -> Void)?
    
    public func sectionInsert(sections: [Int]) { self.sectionInsertFn?(sections) }
    public var sectionInsertFn: ((_ sections: [Int]) -> Void)?
    public func sectionDelete(sections: [Int]) { self.sectionDeleteFn?(sections) }
    public var sectionDeleteFn: ((_ sections: [Int]) -> Void)?
    
    public func updateUI() { self.updateUIFn?() }
    public var updateUIFn: (() -> Void)?
    
    public init(initFn:(RowsObserverSimple) -> Void) {
        initFn(self)
    }
}
