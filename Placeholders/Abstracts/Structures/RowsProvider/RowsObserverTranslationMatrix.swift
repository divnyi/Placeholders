//
//  RowsObserverTranslationMatrix.swift
//  Placeholders
//
//  Created by Oleksii Horishnii on 5/7/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import UIKit

/**
 Class used to synchronize begin/end updates
 */
public class RowsUpdateSynchronizer {
    public var beginUpdateCount = 0
    public var willChangeContentFn: (() -> Void)?
    public var didChangeContentFn: (() -> Void)?
    
    public func willChangeContent() {
        if (beginUpdateCount == 0) {
            self.willChangeContentFn?()
        }
        beginUpdateCount += 1
    }
    
    public func didChangeContent() {
        beginUpdateCount = max((beginUpdateCount - 1), 0)
        if (beginUpdateCount == 0) {
            self.didChangeContentFn?()
        }
    }
}

/**
 Class that is used to translate RowsUpdate with section/row shifts
 */
public class RowsObserverTranslationMatrix: WeakItem, RowsObserver {
    private weak var target: RowsObserver?
    private var sectionShift: ((Int) -> Int)?
    private var rowShift: ((IndexPath) -> IndexPath)?
    private var synchronizer: RowsUpdateSynchronizer?
    
    public init(target: RowsObserver,
                sectionShift: ((Int) -> Int)?,
                rowShift: ((IndexPath) -> IndexPath)?,
                synchronizer: RowsUpdateSynchronizer?) {
        self.target = target
        self.sectionShift = sectionShift
        self.rowShift = rowShift
        self.synchronizer = synchronizer
    }
    
    private func sectionShift(section: Int) -> Int {
        return self.sectionShift?(section) ?? section
    }

    private func itemShift(path: IndexPath) -> IndexPath {
        if let rowShift = self.rowShift {
            return rowShift(path)
        }
        return IndexPath(row: path.row,
                         section: self.sectionShift(section: path.section))
    }
    
    //MARK:- WeakItem
    public func remove() {
        self.target = nil
    }
    
    public func testIfAlive() -> Bool {
        return self.target != nil
    }
    
    public func obtain<Type>() -> Type? {
        return self as? Type
    }

    //MARK:- RowsObserver
    
    public func willChangeContent() {
        if let synchronizer = self.synchronizer {
            synchronizer.willChangeContent()
        } else {
            self.target?.willChangeContent()
        }
    }
    
    public func didChangeContent() {
        if let synchronizer = self.synchronizer {
            synchronizer.didChangeContent()
        } else {
            self.target?.didChangeContent()
        }
    }
    
    public func insert(paths: [IndexPath]) {
        let newPaths = paths.map { self.itemShift(path: $0) }
        self.target?.insert(paths: newPaths)
    }
    
    public func delete(paths: [IndexPath]) {
        let newPaths = paths.map { self.itemShift(path: $0) }
        self.target?.delete(paths: newPaths)
    }
    
    public func update(paths: [IndexPath]) {
        let newPaths = paths.map { self.itemShift(path: $0) }
        self.target?.update(paths: newPaths)
    }
    
    public func sectionInsert(sections: [Int]) {
        let newSections = sections.map { self.sectionShift(section: $0) }
        self.target?.sectionInsert(sections: newSections)
    }
    
    public func sectionDelete(sections: [Int]) {
        let newSections = sections.map { self.sectionShift(section: $0) }
        self.target?.sectionDelete(sections: newSections)
    }
    
    public func updateUI() {
        self.target?.updateUI()
    }
}
