//
//  PlaceholdersRealmUnitTests.swift
//  PlaceholdersRealmUnitTests
//
//  Created by Oleksii Horishnii on 7/26/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import XCTest
import RealmSwift
import Placeholders
import PlaceholdersRealm

class SomeItem: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var date: Date = Date()
    @objc dynamic var name: String = ""
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? SomeItem {
            return self.id == object.id
        }
        return false
    }
}

class PlaceholdersRealmUnitTests: XCTestCase {
    var realm: Realm!

    override func setUp() {
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = "realmTestsDatabase"

        self.realm = try! Realm()
        try! self.realm.write {
            self.realm.deleteAll()
        }
    }
    
    func testCreateSingleItem() {
        let provider = self.realm.objects(SomeItem.self).rowsProvider()
        
        let insertExpectation = expectation(description: "Inserted")
        let listener = RowsObserverSimple { updateDelegate in
            updateDelegate.insertFn = { paths in
                insertExpectation.fulfill()
            }
        }
        let _ = provider.observers.add(listener)

        XCTAssertEqual(provider.toArray(), [[]])
        // add new item
        let item = SomeItem()
        item.id = 123
        item.name = "tratata"
        try! self.realm.write {
            self.realm.add(item)
        }
        XCTAssertEqual(provider.toArray(), [[item]])
        waitForExpectations(timeout: 0.3, handler: nil)
    }
    
    func testCreate10Items() {
        let provider = self.realm.objects(SomeItem.self).rowsProvider()
        
        var expectations: [XCTestExpectation] = []
        for idx in 0..<10 {
            expectations.append(expectation(description: "Inserted at idx = \(idx)"))
        }

        let listener = RowsObserverSimple { updateDelegate in
            updateDelegate.insertFn = { paths in
                for path in paths {
                    let expectation = expectations[path.row];
                    expectation.fulfill()
                }
            }
        }
        let _ = provider.observers.add(listener)
        
        XCTAssertEqual(provider.toArray(), [[]])
        try! self.realm.write {
            for _ in 0..<10 {
                // add new item
                let item = SomeItem()
                item.id = 123
                item.name = "tratata"
                self.realm.add(item)
                XCTAssertEqual(provider.toArray()[0].last, item)
            }
        }
        waitForExpectations(timeout: 0.3, handler: nil)
    }
    
    func testDeleteItems() {
        let provider = self.realm.objects(SomeItem.self).rowsProvider()
        
        let expectationDelete3 = expectation(description: "item with id = 3 deleted")
        let expectationDelete7 = expectation(description: "item with id = 7 deleted")
        let listener = RowsObserverSimple { updateDelegate in
            updateDelegate.deleteFn = { paths in
                for path in paths {
                    if (path.row == 3) {
                        expectationDelete3.fulfill()
                    }
                    if (path.row == 7) {
                        expectationDelete7.fulfill()
                    }
                }
            }
        }
        let _ = provider.observers.add(listener)

        var items: [SomeItem] = []
        for idx in 0..<10 {
            // add new item
            let item = SomeItem()
            item.id = idx
            item.name = "tratata"
            try! self.realm.write {
                self.realm.add(item)
            }
            
            items.append(item)
            
            XCTAssertEqual(provider.toArray()[0].last, item)
        }
        
        try! self.realm.write {
            self.realm.delete(items[3])
            self.realm.delete(items[7])
        }

        waitForExpectations(timeout: 0.3, handler: nil)
    }
}
