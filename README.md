[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](#carthage) [![CocoaPods compatible](https://img.shields.io/cocoapods/v/Placeholders.svg)](#cocoapods) 

![Swift 4.1](https://img.shields.io/badge/Swift-4.1-orange.svg) ![platforms](https://img.shields.io/badge/platforms-iOS-lightgrey.svg)

# Placeholders
Make placeholder views in your storyboard, add views or viewcontrollers later

#### Carthage

If you use [Carthage][] to manage your dependencies, add
Placeholders to your `Cartfile`:

```
git "https://gitlab.com/divnyi/Placeholders.git"
```

Use [this steps](https://github.com/Carthage/Carthage#quick-start) to proceed further.

Also, see `CarthageExample/` sample project, with all dependencies set up.

*Note: interface builder doesn't autocomplete Class and Module from Carthage packets as for now (4 May 2018).  
You need to specify both manually for it to work.*

#### CocoaPods

If you use [CocoaPods][] to manage your dependencies, simply add
Placeholders to your `Podfile`:

```
  pod 'Placeholders'
  pod 'PlaceholdersCoreData'
  pod 'PlaceholdersRealm'
```

See `CocoapodsExample/` sample project.

## Usage

Imagine you want to reuse view or view controller in many places in an application, without re-writing it. What would you do to achieve this?

This library presents a number of simple solutions to this question.

### First of all, we have **PlaceholderView**:

You can put **PlaceholderView** in any place you wish, using constraints or whatever you want to align it.  
Then, pass an actual view you want to see there by setting `placeholderView.insertedView` parameter to your view - and it will insert your view inside **PlaceholderView**.

Simple, eh?

**Q: Wait, didn't you mentioned view controllers before?**  
A: Yeah, you can reuse UIViewController too.  
You can pass whatever implements **AnyView** interface to `.insertedView`, and luckily, both UIView and UIViewController implement **AnyView**.
<details>
  <summary>AnyView diagram</summary>
  
  ![](Readme/AnyView.jpg)
  
</details>

 

**Q: But can't I do this without this library? How hard is that to add subview?**  
A: You can, but it will require more efforts (for ViewControllers - for sure). And don't forget you also need to align everything with constraints.

**Q: So, that's it?**  
A: No, it is just a start :)

### We can also show AnyView in popup!

Who said we can't make Placeholder View Controller for our **PlaceholderView**?  
Well, we can, and we can also add methods to show this view controller on the brand new separate UIWindow, right above application's UIWindow.

And then, we can extend **AnyView** to have `.showAsPopup(level: UIWindowLevel)` method, so you don't even need to know about PlaceholderVC anymore.

**Q: Why did you tell us about it then?**  
A: You might want to dismiss a popup too, so you might want to know who is responsible for that.

### Even **PlaceholderTableVC**. I'm not joking.

But this one is more complex.

Remember a thing about reuseId and why do we even need it?

We want to reuse UI elements that can be reused.
To do that, we need to separate creation of UI item from updating UI item.

Which we do in **CellGenerator**.

<details>
  <summary>CellGenerator diagram</summary>
  
  ![](Readme/CellGenerator.jpg)
  
</details>

 

But this is just one cell, and we need.. how many?  

Comes out, we need Rows and Columns of items.  
And we also don't want to store everything in memory, so it should be lazy.

Meet **RowsProvider\<Type\>**.

<details>
  <summary>RowsProvider diagram</summary>
  
  ![](Readme/RowsProvider.jpg)
  
</details>

 

This simple class unifies our needs - we have columns, rows, items...  
And all of it is lazy, because inside **RowsProvider** is just 3 closures that provide us all information about collection at hand.  
No information is stored directly.

<details>
  <summary>So taken RowsProvider of CellGenerator, we have everything we need to create UITableView automatically.</summary>
  
  ![](Readme/PlaceholderTableVC.jpg)
  
</details>

 

**Q: Wait, that's it? You don't need to write those UITableViewDelegate and UITableViewDataSource anymore?**  
A: Correct.

**Q: But how do you register cells to be reused?**  
They are registered dynamically, on the fly. Each **CellGenerator** has reuseId, remember? If we have new reuseId, we register new cell.

**Q: It sounds too good. This implementation of yours must have performanece issues.**  
A: Sure, we do have some overhead when performing `.transform` chain of our **RowsProvider**s,  
but otherwise, performance is really good - we do call new cells only when we need them,  
and we reuse UI elements.

**Q: If RowsProvider is just a bunch of closures,  
then you still need that data to be allocated in the other  
part of your application -- thus it is still not lazy enough**  
True, but that's if you don't use databases.

**Q: You don't say...**  
A: Yup, we can create RowsProvider from both CoreData or Realm.

## RowsProvider and CoreData

We can connect to CoreData database using `RowsProvider<DatabaseEntity>.from(coreDataParams:)`  
or `RowsProvider<DatabaseEntity>.from(coreDataFetchedResultController:)` methods.

We won't just have an interface to get cells on demand,  
but also observe CoreData changes, like NSFetchedResultsController does.

**RowsProvider** is **RowsObserver** -- it listens for incoming changes.  
But it's also **RowsObserverProxy**, so it doesn't handle changes itself, but rather allows anyone to subscribe for updates.

Luckily, our **PlaceholderTableVC** do, and you don't even need to think about it:  
**RowsProvider** automatically subscribes new **RowsProvider** when you `.transform`

<details>
  <summary>Our applications can look like this</summary>
  
  ![](Readme/RowsProviderFlow.jpg)
  
</details>

More in-depth example:

1. Get RowsProvider with raw coredata object
```swift
        let dbRows = RowsProvider<DBItem>.from(coreDataParams:
            FetchRequestParameters(entityName: "Item",
                                   managedObjectContext: self.managedObjectContext,
                                   sortDescriptors: [NSSortDescriptor(key: "date", ascending: false)]))
```
2. Transform it to plain NSObject model item RowsProvider
```swift
        let itemRows = dbRows.transform { $0.toPONSO() }
```
3. Transform Model item into View Model item, with data relevant to UI
```swift
        let itemModels = itemRows.transform({ (item) -> RowModel in
            let model = RowModel.from(item: item)
            return model
        })
```
4. Also create View Model for static item on top
```swift
        let newItem = RowModel.addItemCell()
        newItem.clicked = {
            useCase.addNewItem()
        }
```
5. Add static item to dynamic items by adding RowsProvicers of same type
```swift
        let rows = RowsProvider.from(singleItem: newItem) + itemModels
```
6. Transform View Model into CellGenerator, that explains how to  
create, update the cell and how to react on cell actions
```swift
        let cellGenerators = rows.transform { (rowModel) -> CellGenerator in
            CellGenerator(create: .generator({ () -> UILabel in
                return UILabel(frame: CGRect.zero)
            }), update: { (label: UILabel, _) in
                label.text = rowModel.text
                label.backgroundColor = rowModel.bgColor
                label.textAlignment = rowModel.alignment == .left ? .left : .center
            }, clicked: rowModel.clicked,
               deleted: rowModel.deleted)
        }
```

It can look like this, and it looks like this! 
Check out **ExampleCoreData** target.

## RowsProvider and Realm

Basically, same as above, except

1. Get RowsProvider from Results<RealmItem>
```swift
        let dbRows = self.realm.objects(RealmItem.self)
            .sorted(by: [SortDescriptor(keyPath: "date", ascending: false)])
            .rowsProvider()
```

And that's it, everything else is the same.  
Steps 2 to 6 doesn't differ.

Check **ExampleRealm** if you don't belive.

[Carthage]: https://github.com/Carthage/Carthage
[CocoaPods]: https://cocoapods.org/

