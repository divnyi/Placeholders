//
//  PlaceholdersCoreDataInterface.swift
//  PlaceholdersCoreData
//
//  Created by Oleksii Horishnii on 9/14/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import UIKit
import CoreData
import Placeholders

extension RowsProvider where Type: NSManagedObject {
    /// Simplified method, using FetchRequestParameters
    public class func from(coreDataParams: FetchRequestParameters) -> RowsProvider<Type> {
            return CoreDataSubject<Type>.rowsProvider(params: coreDataParams)
    }
}

extension RowsProvider {
    /// Using NSFetchedResultsController<Type> to create RowsProvider<Type>
    public class func from<Element>(coreDataFetchedResultController: NSFetchedResultsController<Element>) -> RowsProvider<Element>
        where Element: NSManagedObject {
            return CoreDataSubject<Element>.rowsProvider(fetchedResultController: coreDataFetchedResultController)
    }
}

/// This class is simple way to describe NSFetchedResultsController
public struct FetchRequestParameters {
    public var entityName: String
    public var managedObjectContext: NSManagedObjectContext
    public var sortDescriptors: [NSSortDescriptor]
    public var predicate: NSPredicate?
    public var fetchBatchSize: Int
    public var sectionNameKeyPath: String?
    public init(entityName: String,
                managedObjectContext: NSManagedObjectContext,
                sortDescriptors: [NSSortDescriptor],
                predicate: NSPredicate? = nil,
                fetchBatchSize: Int? = nil,
                sectionNameKeyPath: String? = nil) {
        self.entityName = entityName
        self.managedObjectContext = managedObjectContext
        self.sortDescriptors = sortDescriptors
        self.predicate = predicate
        self.fetchBatchSize = fetchBatchSize ?? 20
        self.sectionNameKeyPath = sectionNameKeyPath
    }
}
