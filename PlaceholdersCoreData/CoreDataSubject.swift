//
//  CoreDataObserver.swift
//  PlaceholdersCoreData
//
//  Created by Oleksii Horishnii on 5/7/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import Foundation
import CoreData
import Placeholders

extension FetchRequestParameters {
    func fetchResultController<Element>(type: Element.Type) -> NSFetchedResultsController<Element> where Element: NSManagedObject {
        let request = NSFetchRequest<Element>(entityName: self.entityName)
        request.predicate = self.predicate
        request.fetchBatchSize = self.fetchBatchSize
        request.sortDescriptors = self.sortDescriptors
        
        let fetchedResultsController = NSFetchedResultsController<Element>(fetchRequest: request, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: self.sectionNameKeyPath, cacheName: nil)
        
        return fetchedResultsController
    }
}

open class CoreDataSubject<Type>: NSObject, NSFetchedResultsControllerDelegate where Type: NSManagedObject {
    //MARK:- creation methods
    public class func rowsProvider(params: FetchRequestParameters) -> RowsProvider<Type> {
        let fetchedResultController = params.fetchResultController(type: Type.self)
        return self.rowsProvider(fetchedResultController: fetchedResultController)
    }
    
    public class func rowsProvider(fetchedResultController: NSFetchedResultsController<Type>) -> RowsProvider<Type> {
        let observer = CoreDataSubject(fetchedResultController: fetchedResultController)
        
        return observer.setup()
    }
    
    //MARK:- private stuff
    private var controller: NSFetchedResultsController<Type>
    
    init(fetchedResultController: NSFetchedResultsController<Type>) {
        self.controller = fetchedResultController
        
        do {
            try fetchedResultController.performFetch()
        } catch {
            abort()
        }
        
        super.init()
        
        fetchedResultController.delegate = self
    }
    
    weak var rows: RowsProvider<Type>?
    private func setup() -> RowsProvider<Type> {
        let rows = RowsProvider(sections: { () -> Int in
            return self.controller.sections?.count ?? 0
        }, rows: { (section) -> Int in
            if let sections = self.controller.sections {
                if section < sections.count {
                    return sections[section].numberOfObjects
                }
            }
            return 0
        },
           item: { (indexPath) -> Type in
            let obj = self.controller.object(at: IndexPath(row: indexPath.row, section: indexPath.section))
            return obj
        })

        self.rows = rows
        return rows
    }
    
    private var deletions: [IndexPath] = []
    private var insertions: [IndexPath] = []
    private var updates: [IndexPath] = []
    private var sectionDeletions: [Int] = []
    private var sectionInsertions: [Int] = []
    func resetChanges() {
        self.deletions = []
        self.insertions = []
        self.updates = []
        self.sectionDeletions = []
        self.sectionInsertions = []
    }
    
    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.resetChanges()
        self.rows?.willChangeContent()
    }
    
    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.rows?.insert(paths: self.insertions)
        self.rows?.delete(paths: self.deletions)
        self.rows?.update(paths: self.updates)
        self.rows?.sectionInsert(sections: self.sectionInsertions)
        self.rows?.sectionDelete(sections: self.sectionDeletions)
        
        self.rows?.didChangeContent()
        
        self.resetChanges()
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                           didChange anObject: Any,
                           at indexPath: IndexPath?,
                           for type: NSFetchedResultsChangeType,
                           newIndexPath: IndexPath?) {
        var type = type
        if (type == .update && newIndexPath != nil && indexPath?.compare(newIndexPath!) != .orderedSame) {
            type = .move;
        }
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                self.insertions.append(indexPath)
            }
        case .delete:
            if let indexPath = indexPath {
                self.deletions.append(indexPath)
            }
        case .update:
            if let indexPath = indexPath {
                self.updates.append(indexPath)
            }
        case .move:
            if let oldIndexPath = indexPath,
                let newIndexPath = newIndexPath {
                self.deletions.append(oldIndexPath)
                self.insertions.append(newIndexPath)
            }
        }
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                           didChange sectionInfo: NSFetchedResultsSectionInfo,
                           atSectionIndex sectionIndex: Int,
                           for type: NSFetchedResultsChangeType) {
        switch (type) {
        case .insert:
            self.sectionInsertions.append(sectionIndex)
            break
        case .delete:
            self.sectionDeletions.append(sectionIndex)
            break
        default:
            break
        }
    }
}
