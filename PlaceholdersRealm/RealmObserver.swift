//
//  RealmObserver.swift
//  PlaceholdersRealm
//
//  Created by Oleksii Horishnii on 7/11/18.
//  Copyright © 2018 Oleksii Horishnii. All rights reserved.
//

import Foundation
import RealmSwift
import Placeholders

extension Results {
    public func rowsProvider() -> RowsProvider<Element> {
        let rowsProvider = RowsProvider<Element>.from(realmResults: [self])
        return rowsProvider
    }
}

extension RowsProvider {
    public class func from<Element>(realmResults:[Results<Element>]) -> RowsProvider<Element> {
        weak var weakRowsProvider: RowsProvider<Element>? = nil
        let notificationTokens = realmResults.enumerated().map { (idx, section) -> NotificationToken in
            let notificationToken = section.observe { (changes: RealmCollectionChange) in
                switch changes {
                case .update(_, let deletions, let insertions, let modifications):
                    weakRowsProvider?.willChangeContent()
                    weakRowsProvider?.insert(paths: insertions.map({ IndexPath(row: $0, section: idx) }))
                    weakRowsProvider?.delete(paths: deletions.map({ IndexPath(row: $0, section: idx) }))
                    weakRowsProvider?.update(paths: modifications.map({ IndexPath(row: $0, section: idx) }))
                    weakRowsProvider?.didChangeContent()
                default:
                    break;
                }
            }
            return notificationToken
        }
        let rowsProvider = RowsProvider<Element>(sections: { () -> Int in
            // hack to retain notificationTokens as long as rowsProvider exists
            return notificationTokens.count
        }, rows: { (section) -> Int in
            return realmResults[section].count
        }) { (indexPath) -> Element in
            return realmResults[indexPath.section][indexPath.row]
        }
        weakRowsProvider = rowsProvider
        return rowsProvider
    }
}
