Pod::Spec.new do |s|
  s.name              = 'Placeholders'
  s.version           = '0.1.3'
  s.summary           = 'Make placeholder views in your storyboard, add views or viewcontrollers later'

  s.author            = { 'Oleksii Horishnii' => 'oleksii.horishnii@gmail.com' }
  s.license           = { :type => 'WTFPL', :file => 'LICENSE' }
  s.homepage          = "https://gitlab.com/divnyi/Placeholders"

  s.platform          = :ios


  s.source = {
    :git => "https://gitlab.com/divnyi/Placeholders.git",
    :tag => "#{s.version}"
  }
  s.source_files = 'Placeholders/**/*.swift'
  s.ios.deployment_target = '8.0'

end
